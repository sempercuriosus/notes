# Notes About Programming

<!-- Title  -->

This contains notes about what I am learning

---

## Links To Notes

- [Terminal](/terminal/terminal.md)
- [Git](/git/git.md)
- [HTML](/html/html.md)
- [CSS](/css/css.md)
- [Javascript](/javascript/javascript.md)
- [Python](/python/python.md)
- [Terminal](/terminal/terminal.md)
- [Web API](/webapi/webapi.md)

---
