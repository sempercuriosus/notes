# CSS Cheatsheet

## CSS Brief

- CSS - Cascading Style Sheets - are the style that get applied to web pages
- A rule can be created by using a **selector** and picking a **property** and giving a **value**
  - `selector {property: value;}`
- Box Model - describes the areas around an up to the content in terms of a box
  - From the inside out
    - **Content** - The content of the box
    - **Padding** - Clears an area around the content it is not displayed
    - **Border** - A border that goes around the padding
    - **Margin** - Clears an area outside the border, between the elements and it is transparent
- **Media Queries** - allow for styles to be conditionally applied based on the device's viewport, screen resolution, or media type `screen` or `print`
  - they have a media type, with one or more media feature expressions and can be combined with logical operators
  - @media mediaType and (mediaFeature: value) {
    /_ Styles to apply for the specified media conditions _/
    }
  - Media types
    - `all`- Applies to all devices and
    - `screen`- Applies to screens, such as computer monitors and mobile
    - `print`- Applies when the content is being
    - `speech` Applies when the content is being spoken by a screen reader.
  - Media Features: Media features define specific conditions based on device characteristics, such as screen size, resolution, orientation, and more.
    - Eg
      - `width` Specifies the width of the viewport.
      - `height` Specifies the height of the viewport.
      - `orientation` Specifies the orientation of the device (landscape or portrait).
      - `max-width` Specifies the maximum width of the viewport.
      - `min-width` Specifies the minimum width of the viewport.
    - `Values` Media features can be combined with values to define specific conditions. For example:
      - `max-width` -`768px` - Applies styles when the viewport width is less than or equal to 768 pixels.
      - `orientation`- `landscape` - Applies styles when the device is in landscape orientation.
    - **Multiple Conditions** - Multiple media conditions can be combined using logical operators like and and or to create more complex queries. For example:

## CSS Selectors

Selectors identify the item(s) that a rule will be applied to; more than one can be applied per rule

- Selector Examples
  - `Element` Selects elements based on their HTML tag name.
    - Eg: section selects all `<section>` elements.
  - `Class` Selects elements with a specific class attribute value
    - Eg: `.class_name` selects all elements with `class="class_name"`
  - `ID` Selects an element with a specific ID attribute value.
    - Eg: `#unique_id` selects the element with `id="unique_id"`
  - `Attribute` Selects elements based on their attribute values.
    - Eg: `[type="text"]` selects all elements with `type="text"`
  - `Descendant` Selects elements that are descendants of another element.
    - Eg: div p selects all `<p>` elements inside `<div>` elements.
  - `Child` Selects elements that are direct children of another element.
    - Eg: `<ul> li` selects all `<li>` elements that are direct children of `<ul>` elements.
  - `Adjacent Sibling` Selects an element that immediately follows another element.
    - Eg: `h2 + p` selects the `<p>` element that directly follows an `<h2>` element.
  - `General Sibling` Selects elements that follow another element.
    - Eg: h2 ~ p selects all `<p>` elements that follow an `<h2>` element.

### Custom selectors

Also known as CSS attribute selectors, allow you to select elements based on specific attribute values or patterns. They provide a powerful way to target elements that meet certain criteria. Here are some examples of custom selectors:

- `Attribute `Selector: Selects elements based on the presence of a specific attribute.
- `[attribute]` selects elements that have the specified attribute, regardless of its value. Example: `[disabled]` selects all elements with the disabled attribute.
- `[attribute=value]` selects elements with the specified attribute and value. Example: `[type="text"]` selects all elements with type="text".
- `[attribute~=value]` selects elements where the attribute value is a space-separated list containing the specified value. Example: `[class~="red"]` selects elements with the class attribute containing the word "red".
- `Substring Matching `Selector: Selects elements based on partial attribute values.
- `[attribute^=value]` selects elements where the attribute value starts with the specified value. Example: `[href^="https://"]` selects all elements with an href attribute starting with "https://".
- `[attribute$=value]` selects elements where the attribute value ends with the specified value. Example: `[src$=".png"]` selects all elements with a src attribute ending with ".png".
- `[attribute*=value]` selects elements where the attribute value contains the specified value. Example: `[title*="example"]` selects all elements with a title attribute containing the word "example".
- `Attribute Value `Selector: Selects elements based on attribute values that match a specified pattern.
- `[attribute|=value]` selects elements where the attribute value is an exact match or starts with the specified value followed by a hyphen. Example: `[lang|="en"]` selects all elements with a lang attribute set to "en" or starting with "en-".
- `[attribute~=value]` selects elements where the attribute value is a space-separated list that includes the specified value. Example: `[class~="active"]` selects all elements with the class attribute containing the word "active".

Custom selectors provide flexibility and allow you to target elements based on specific attribute conditions or patterns. They can be useful in various scenarios, such as styling specific elements or applying behavior based on attribute values. It's important to use custom selectors judiciously and consider browser compatibility and performance implications when using complex attribute selectors.

## Commonly Used Property-Value Pairs:

- **Background**: background-color, background-image, background-repeat, background-position, background-size.
- **Color**: color.
- **Font**: font-family, font-size, font-weight, font-style, text-align.
- **Margin and Padding**: margin, padding.
- **Border**: border, border-width, border-color, border-radius.
- **Width and Height**: width, height.
- **Display**: display, flex, grid.
- **Positioning**: position, top, right, bottom, left.
- **Box Model**: box-sizing, margin, padding, border.
- **Text**: text-decoration, text-transform, text-align, line-height.
- **Animation**: animation, transition.
- **Flexbox**: flex-direction, justify-content, align-items, flex-grow, flex-shrink.
- **Grid**: grid-template-columns, grid-template-rows, grid-gap, grid-column, grid-row.

Remember, this is just a brief overview of CSS selectors and commonly used property-value pairs. CSS is a powerful styling language with many more selectors, properties, and values available. It's important to refer to the CSS documentation and resources for more in-depth information and examples.

---

# Flexbox

Is a one-dimensional layout model which allows flexible and dynamic distribution of space between `flex-items` offering properties and alignment capabilities to control the position and alignment within a flex container

- **Main-Axis** - Primary axis along which content is defined
- **Cross-Axis** - Secondary axis along which content is defined

## Flex Container Properties:

- `display: flex;` - Defines a flex container.
- `flex-direction: row;` - Sets the direction of the flex items in the container (row, row-reverse, column, column-reverse).
- `flex-wrap: nowrap;` - Specifies whether the flex items should wrap or not
  - `nowrap`
  - `wrap`
  - `wrap-reverse`
- `justify-content: flex-start;` - Aligns the flex items along the main axis
  - `flex-start`
  - `flex-end`
  - `center`
  - `space-between`
  - `space-around`
- `align-items: stretch;` - Aligns the flex items along the cross axis
  - `stretch`
  - `flex-start`
  - `flex-end`
  - `center`
  - `baseline`
- `align-content: stretch;` - Aligns the flex lines along the cross axis when there is extra space
  - `stretch`
  - `flex-start`
  - `flex-end`
  - `center`
  - `space-between`
  - `space-around`

## Advanced Flex Container Properties:

- `flex-flow: row wrap;` - Shorthand for flex-direction and flex-wrap.
- `align-items: flex-start;` - Aligns the flex items at the start of the cross axis.
- `align-items: flex-end;` - Aligns the flex items at the end of the cross axis.
- `align-items: center;` - Aligns the flex items at the center of the cross axis.
- `align-items: baseline;` - Aligns the flex items at their baselines.
- `align-content: space-between;` - Distributes the flex lines evenly with space between them.
- `align-content: space-around;` - Distributes the flex lines evenly with space around them.

## Flex Item Properties:

- `flex-grow: 0;` - Specifies how much the flex item should grow relative to the other items.
- `flex-shrink: 1;` - Specifies how much the flex item should shrink relative to the other items.
- `flex-basis: auto;` - Specifies the initial size of the flex item.
- `flex: 0 1 auto;` - Shorthand for `flex-grow`, `flex-shrink`, and `flex-basis`
- `order: 0;` - Specifies the order in which the flex items are displayed.
- `align-self: auto;` - Overrides the align-items value for a specific flex item.

## Advanced Flexbox Examples:

**Equal-height Columns:**

```
.container {
display: flex;
}
```

```
.column {
flex: 1;
}
```

**Centered and Spaced-out Navigation:**

```
.container {
display: flex;
justify-content: space-between;
align-items: center;
}
```

```
.nav-item {
margin: 0 10px;
}
```

**Responsive Card Layout:**

```
.container {
display: flex;
flex-wrap: wrap;
}
```

```
.card {
flex: 1 0 300px;
}
```
