# Git Commands

2023 Cheatsheet

---

## Config

All of these commands will start with `git`

- `config`
  - `-l` - lists your configurations in a list
  - `--global user.name "Username Here"` - sets your username
  - `--global user.email "Email Here` - sets your email

---

## Basic Commands

All of these commands will start with `git`

- `init` - initializes a new local repo in your working dir
- `add [filename here]` - adds a file to the staging area
  - `.` adds all files into the staging area
  - `*` adds files matching the criteria
    - eg `git add test*` adds the files starting with test
- `status` - checks the status of the repo
- `commit` - opens a text editor to write a full commit message
  - `m` - adds a message without the editor opening
  - `a` - skips the staging area and commits
    - `git commit -a -m "message"`
- `diff` - shows the differences before commit
- `log` - shows commits history including all files and changes
- `show commit-id` - shows a commit for a commit-id found in the log
- `rm` - removes a tacked file and expects a message explaining why
- `checkout [filename here]`- reverts un-staged changes
- `reset HEAD [filename here]`- reverts staged changes
- `commit --amend`- modify or add changes to the most recent commit
- `revert` - creates a new commit that is the opposite of everything in the given commit
  - `git revert HEAD`- reverts the las commit by using the HEAD alias
- `revert [commit_id_here]` - reverts an old commit using its id
- `mv` - move - inside of a dir being tracked with Git, **ALWAYS** use the Git move command to preserve the .git data
  - `git mv [source] [target]`
  - If one just does a move, then Git will not track them outside of thinking that the location moved from has a deleted file, and the location moved to will see a new file.

---

## Branch Commands

All of these commands will start with `git`

- `branch` - lists all the branches
- `branch [branch_name]` - creates a branch
  - `d` with a `[branch_name]` deletes a branch
    - the intent is this is done after one is complete with it and the branch is merged
  - `M` - is the name of a new branch
- `checkout` - switches to a new branch
  - `git checkout -b "branch_name"` - creates and switches to a new branch
- `merge [branch_name]` - merges the branch you are in with the target `branch_name`
  ----push
  ##Remote Commands
  All of these commands will start with `git`
- `push` - saves your local changes to the remote repo
  - `--set-upstream-to [remote_branch]` - this sets the remote repo
    - `u` is shorthand for this with a `remote_branch` and a `local_branch` set
      - `git push -u origin local-branch`
  - `--delete [remote_branch] [branch_name_here]`
- `pull` - gets changes from remote to local and merge them
- `fetch` - gets the changes from remote to local but does **not** merge them
- `remote -v` - shows all remote repos
- `branch -r` - shows all remote branches currently tracked

---

## Advanced Commands

- `rebase`

---

## Putting It Together

### Create New Repo And Setup to Link

git init (create a new repo)
git add . (adding all files)
git status (good practice to check)
git commit -m "" (add a message to the files we want to push)

### Linking Local and Remote

git remote add origin <the HTTPS or SSH URL ending in .git>
git branch -M main
git push -u origin main
