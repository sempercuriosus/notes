# HTML Cheatsheet

## HTML Basic Thoughts

- **HTML** Hyper Text Markup Language - is a building framework to create the outline of a webpage, built by blocks
- When a webpage loads, it loads top to bottom
- HTML **elements** are defined as having an opening and a closing **tag**, generally, contain some type of **content** **OR** perform an **action**, and may contain **attributes**
  - Opening `<Tag>`
  - Closing `</Tag>`
  - Self-Closing or Void `<Tag/>`
- **Attributes** live within the opening tag, are similar to adjectives, in the sense that they describe the element; all elements can accept attributes
  - Eg
    - `alt`
    - `href`
    - `placeholder`
    - `src`
    - `value`
- **Global Attributes** can be used on _any_ HTML element, but may not impact all elements
  - Eg
    - `id`
    - `class`
    - `dir`
    - `lang`
    - `style`
- **Custom Attributes** `data-` prefix - defines a custom attribute (see below)
- All together this is an element
  `<Tag attribute="value">Content</Tag>`
- **Inline Elements**
  - Do not start a new line and take up only as much room as needed for the content, and can start on the same line as other elements
    - Eg
      - `span`
      - `img`
      - `label`
      - `button`
- **Block Elements**
  - Always start a new line and take up the full available width regardless of content size, and cannot start on the same line as another piece of content, without styling.
    - Eg
      - `p`
      - `section`
      - `div`
      - `aside`

## Web Notes

- The Browser's job is to interpret what the developer creates in HTML, CSS, Javascript, etc.
- Not all Browsers can interpret all things, and sometimes their interpretation varies

## Structuring a Web Page:

- **Head** - The `<head>` contains non-user displayed items such as metadata and the resources that are needed by the page.
  - Eg CSS, Javascript
- **Body** - The `<body>` contains the content of the web page displayed (rendered) to the user. This can include a LOT of elements used to create the page.
  - Eg text, images, forms
- **Dividing the Page** - Divide your page into logical sections using appropriate HTML elements. For example, you can use <header> for the top section of your page, <nav> for the navigation menu, <main> for the main content area, <footer> for the footer section, and <section> or <article> for other content sections.
  - Eg the `<Header>` will have the elements and content on the top of the page.
  - The `<Footer>`, similarly, has elements and content at the bottom of the page.
  - The `<Nav>` contains the navigation menu elements
  - `<Main>` is the main area which elements and content are contained, and will sometimes have an `<Aside>` which has similar content but off to one side
  - `<Section>` or `<Article>` are distinct divisions of content which live in the `<Main>`, each `<Section>` or `<Article>` can be divided by a `<Div>` to break up other contained elements.

## Basic Structure

<!DOCTYPE html>
<html>
<head>
    <title>Page Title</title>
	<metadata></metadata>
</head>
<body>
    <!-- Content goes here -->
</body>
</html>

## Headings

**Headings** `<h1> to <h6>`- Use headings to structure your content and provide hierarchy.

- Use `<h1>` for the main heading of the page
- Use subsequent headings `<h2>`, `<h3>`, etc. for subheadings.

<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<h6>Heading 6</h6>

## Paragraphs

**Paragraphs** `<p>` the paragraph element separates blocks of text and provides a visual separation between the different sections of content presented.

`<p>This is a paragraph.</p>`

## Links

**Links** `<a>` - Anchor - Define clickable links to sections, other pages, or resources.

- The `href` hyperlink reference - attribute to specify the URL that the link should navigate to. The target attribute to controls how the link opens either in a new tab or in the same window replacing the current view.

`<a href="https://www.example.com">Link Text</a>`

## Images

**Image** `<img>` - Create an area in which images are presented.

- The `src` source - attribute sets the image source (URL or file path)
- The `alt` alternate - attribute provides alternative text describing the image.
  - Alt text is important for accessibility and search engine optimization.

`<img src="image.jpg" alt="Image Description">`

## Lists

**Lists** have two flavors in the form of an unordered list `<ul>` and an ordered list `<ol>`

**List Item**`<li>` define an item that will appear on the list; used for both flavors.

**Unordered List:**

<ul>
    <li>Item 1</li>
    <li>Item 2</li>
    <li>Item 3</li>
</ul>
**Ordered List:**
<ol>
    <li>Item 1</li>
    <li>Item 2</li>
    <li>Item 3</li>
</ol>

## Tables

**Tables** `<table>` organize tabular data in a more presentable formatting

- The `<table>` element creates the structure
  - `<th>` make a **t**able **h**eader
  - `<tr>` make a **t**able **r**ow
  - `<td>` make a **t**able cell (**d**ata) for the content

<table>
    <tr>
        <th>Header 1</th>
        <th>Header 2</th>
    </tr>
    <tr>
        <td>Data 1</td>
        <td>Data 2</td>
    </tr>
</table>

## Forms

**Forms** `<Form>` Collect user input in a specified/ structured way.

- `<input>` has various types of input fields
  - e.g., text, email, password
- `<label>` provides a text label for the input field to describe the content asked for
- `<button>` submit buttons or other types of buttons within the form to trigger an action, provide an option, etc.

<form>
    <label for="name">Name:</label>
    <input type="text" id="name" name="name">
    <input type="submit" value="Submit">
</form>

## Section

**Section** `<Section>` - Is a semantic logical division to contain a standalone section of the page; mostly replaced the `<Div>` as a way to break up the page.

<Section>
    <!-- Content goes here -->
</Section>

## Article

**Article** `<Article>` - Is a semantic logical division to contain an individual post, article, blog entry, product card, etc. they are intended to be independently distributable or reusable.

<Article>
    <!-- Content goes here -->
</Article>

## Aside

**Aside** `<Aside>` - Is a semantic part a document, in which, the content is indirectly related to the pages's main content and are often sidebars or call-out boxes to highlight something.

<Aside>
    <!-- Content goes here -->
</Aside>

## Divisions

**Divisions** `<div>` - Is a container element that can be used to wrap and style multiple elements together into a single block.

<div>
    <!-- Content goes here -->
</div>

## CSS Styles

**CSS** `<style>` - Element to apply custom styles to your HTML elements in the page

<style>
    /* CSS styles go here */
</style>

## When Not to Use HTML Elements:

- Avoid using outdated or deprecated elements
  - Eg: `<font>`, `<center>`, or `<strike>` are outdated
  - use CSS to style your content namely, this allows more flexibility and is a separation of concerns.
- Avoid using non-semantic elements
  - Non-semantic elements like `<span>` or `<div>` don't provide any specific meaning or structure to your content.
- It is better to use more specific semantic elements that convey the content's purpose
  - Eg: `<header>`, `<nav>`, `<article>`, `<section>`, etc.
- Avoid excessive nesting of elements
  - Keeping your HTML structure clean and minimal is important for readability and maintainability.
  - Avoid unnecessary nesting of elements, as it can make your HTML more complex and harder to understand.
  - Use elements only when they provide meaningful structure or functionality to your content.

# Questions I asked

## Question

- To define a custom HTML attribute, you can use the data- prefix followed by your desired attribute name. Here's an example:

## Answer

- `<div data-custom-attribute="value">...</div>`
  - In this example, data-custom-attribute is a custom attribute that you can use to store additional information or data related to the element. You can replace "custom-attribute" with any name you prefer. Just make sure to use lowercase letters and hyphens to separate words in the attribute name.

---
