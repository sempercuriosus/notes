# Javascript Cheatsheet

## JavaScript Language Basics:

- **Variables:** var, let, const
- **Data Types:** string, number, boolean, null, undefined, object, symbol
- **Operators:** arithmetic, assignment, comparison, logical, ternary
- **Control Flow:** if statement, switch statement, for loop, while loop, do-while loop
- **Functions:** function declaration, function expression, arrow functions
- **Arrays:** creating arrays, accessing elements, array methods (e.g., push(), pop(), forEach(), map(), filter(), reduce())
- **Objects:** object literals, accessing properties, object methods, object destructuring
- **Error Handling:** try-catch statement, throwing and catching errors

## JavaScript Best Practices:

- Use meaningful variable and function names
- Follow consistent indentation and code formatting
- Comment your code to improve readability and explain complex logic
- Avoid global variables and use local scope whenever possible
- Use strict mode ('use strict') to enforce stricter rules and prevent common mistakes
- Avoid using eval() and with statements
- Use semicolons to end statements (although they are optional in most cases)
- Avoid modifying built-in objects' prototypes
- Use === for strict equality comparison instead of ==
- Avoid unnecessary code repetition and strive for code reusability
- Regularly test and debug your code to catch errors early

## Program Structure:

- **Entry Point:** The starting point of a JavaScript program is usually the main() function or the global scope.
- **Modules:** Use modules to organize code into separate files and improve maintainability. Modules can be imported and exported using the import andexport keywords.

- **Asynchronous Programming:** Use promises, async/await, or callbacks to handle asynchronous operations and avoid blocking the main thread.
- **Error Handling:** Properly handle errors using try-catch blocks or by returning rejected promises.
- **Code Organization:** Group related functions and variables together. Use meaningful file and folder names to organize your codebase.
  Function Structure:

- **Function Declaration:** function functionName(parameters) { // code }
- **Function Expression:** const functionName = function(parameters) { // code }
- **Arrow Functions:** (parameters) => { // code }
- **Default Parameters:** function functionName(parameter = defaultValue) { // code }
- **Rest Parameters:** function functionName(...parameters) { // code }
- **Function Invocation:** functionName(arguments)

## Good-to-Knows:

- JavaScript is a dynamically typed language, meaning that variables can hold values of any type.
- JavaScript is single-threaded, but it supports asynchronous programming through mechanisms like timers, callbacks, and promises.
- JavaScript has a built-in garbage collector that automatically frees up memory by removing unused objects.
- JavaScript has a large standard library, including built-in objects like Math, Date, Array, String, and more.
- JavaScript can be run in web browsers, on the server-side with Node.js, and on various other platforms.
- JavaScript is often used for client-side web development, server-side development, and building cross-platform mobile applications using frameworks like React Native and Ionic.

Remember, this is just a brief overview of JavaScript and its best practices. JavaScript is a versatile and powerful language, and there is much more to explore. Continuously learning and practicing JavaScript will help you become more proficient and efficient in your coding.

# Questions I asked

- What is the difference between `null` and `undefined`?
  - In summary, `undefined` is used when a variable has not been assigned a value, while `null` is used to explicitly indicate the absence of an object value. `Undefined` is a primitive value that is automatically assigned to a variable that has been declared but has not been assigned a value. `null` is an assignment value that represents the intentional absence of any object value.

---

- Why use a function expression over regular function declarations?

  - There are a few reasons why you might choose to use a function expression over a regular function declaration in JavaScript:

    - Function scope: Function expressions allow you to define functions within the scope of another function. This can be useful for encapsulating functionality and avoiding naming conflicts.

    - Closures: Function expressions can create closures, which allow inner functions to access variables from their outer function. This can be helpful for maintaining private variables or creating modular code.

    - Asynchronous programming: Function expressions are commonly used in asynchronous programming, such as with callback functions or promises. They allow you to define functions inline and pass them as arguments to other functions.

    - Code organization: Function expressions can help improve code organization by allowing you to define functions at the point where they are needed, rather than at the top of a script or module.

  It's important to note that both function expressions and function declarations have their own use cases, and the choice between them depends on the specific requirements of your code.

---
