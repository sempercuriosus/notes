# Terminal Commands

This is a list of commonly referred to commands, and some that I found that were worth noting.

---

- `pwd` - print working directory - lists the current location you are in the terminal
- `sudo` - su do runs a command as root.
- `!!` returns the previous command
  - commonly used as `sudo !!`
- `ls` - lists the contents of the target directory.
  - `[directory]` can be just the working directory or another specified one.
  - `[optional flags]`
    - `l`- long format in a table with columns
    - `a`- all files or directories including hidden
    - `d` - list only directories
    - `*` - list with the contents of the sub directories
    - `R` - (note the case) recursively list all files and directories
    - `t` - sorts by last modified date in descending order
    - `S` - (note the case) list by size in descending order
    - `r` - (note the case) reverse flag
- `cd` - change directory
  - `..` to go up a directory
  - `[directory]` specifies where you want to go
  - when using a path
    - `.` is the current folder
    - `/` is the root folder
- `mv [source] [destination]` - move takes a source and a destination and moves the target file or directory
  - there can be more than one source
    - `mv pear apple fruits`
      - pear and apple moved to the fruits folder
- `cp` - copy a file use the `r` switch to include the contents recursively
- `open` - opens a file or directory
  - `open .` opens the current directory in a finder window
- `touch` - create and empty file with extension
- `mkdir` - make directory
  - you can create multiple folders with one command or create multiple nested folders with the `-p` switch.
    - `mkdir [folder1] [folder2]` etc. these are made inside the same location you are in currently as siblings
    - `mkdir -p [folder1/folder2]`etc. these are made as sub directories
- `rmdir` - remove directory - the directory must be empty
  - you can remove multiple folders with one command or a path may be specified
    - `rmdir [folder1] [folder2]`
- `rm` - remove to delete folders with files in them
  - the `rf` switch is needed for this and there is bin or confirmation when deleting, so use care here.
