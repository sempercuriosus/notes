# WEB API Summary

## Web API Functionality:

Web APIs (Application Programming Interfaces) allow different software applications to communicate and interact with each other over the internet.

- They provide a set of rules and protocols for how different applications can request and exchange data.
- Web APIs enable developers to access and use the functionality and data of other applications or services, such as social media platforms, weather services, payment gateways, and more.
- They are typically accessed using HTTP (Hypertext Transfer Protocol) requests, such as GET, POST, PUT, DELETE, etc.

### Attributes:

Attributes are properties or characteristics that can be assigned to HTML elements to provide additional information or functionality.

- In the context of web APIs, attributes can be used to specify parameters or options for API requests.
- For example, when making an API request to retrieve weather data, you might specify attributes like the location, units of measurement, or the desired time period for the forecast.

### Events:

Events are actions or occurrences that happen in the browser or on a web page, such as a button click, mouse movement, or form submission.

- In the context of web APIs, events can be used to trigger specific actions or functions when certain conditions are met.
- For example, you can use events to listen for user interactions and then make API requests or update the UI based on those interactions.

### Event Types:

Web APIs support various types of events, including:

- Mouse events: such as click, hover, mouseover, etc.
- Keyboard events: such as keydown, keyup, keypress, etc.
- Form events: such as submit, input, change, etc.
- Network events: such as load, error, progress, etc.
- Custom events: developers can create and dispatch their own custom events.

### Common Event Types

- `click` - Fired when a mouse button is clicked on an element.
- `submit` - Fired when a form is submitted.
- `keydown` - Fired when a key is pressed down.
- `keyup` - Fired when a key is released.
- `mouseover` - Fired when the mouse pointer moves over an element.
- `mouseout` - Fired when the mouse pointer moves out of an element.
- `load` - Fired when a resource (such as an image or script) finishes loading.
- `error` - Fired when an error occurs while loading a resource.
- `scroll` - Fired when an element's scrollbar is scrolled.
- `resize` - Fired when the browser window is resized.

### Intervals:

Intervals are a feature of web APIs that allow you to execute a function repeatedly at a specified time interval.

- The setInterval() function is commonly used to schedule the execution of a function at regular intervals.
- You can specify the time interval in milliseconds, and the function will be called repeatedly until you stop it using the clearInterval() function.

### Other Functionality:

Web APIs provide many other functionalities, such as:

- DOM manipulation: APIs like the Document Object Model (DOM) allow you to access and modify the structure and content of web pages.
- Geolocation: APIs like the Geolocation API allow you to retrieve the user's current location.
- Storage: APIs like the Web Storage API and IndexedDB allow you to store and retrieve data locally on the user's device.
- Canvas: APIs like the Canvas API allow you to draw and manipulate graphics and animations.
- Audio and Video: APIs like the Audio and Video APIs allow you to play and control audio and video content.

## API Documentation:

- Each web API has its own documentation that provides detailed information about its endpoints, parameters, response formats, authentication requirements, and usage guidelines.
- It is crucial to read and understand the API documentation thoroughly to effectively use the API and avoid any potential issues.

## Authentication and Authorization:

- Many web APIs require authentication to ensure that only authorized users or applications can access their resources.
- Common authentication methods include API keys, OAuth, and token-based authentication.
- You may need to register your application with the API provider and obtain credentials (such as API keys) to authenticate your requests.

## Rate Limiting:

- Web APIs often impose rate limits to prevent abuse and ensure fair usage by limiting the number of requests a client can make within a specific time period.
- It is important to be aware of the rate limits imposed by the API and design your application accordingly to handle rate limit restrictions.

## Error Handling:

- Web APIs can return various types of errors, such as authentication errors, invalid requests, server errors, etc.
- It is important to handle these errors gracefully in your application by checking for error responses and providing appropriate feedback to the user.

## Cross-Origin Resource Sharing (CORS):

- CORS is a security mechanism implemented by web browsers to restrict cross-origin requests made by client-side scripts.
- If you are making API requests from a web page hosted on a different domain than the API server, you may encounter CORS restrictions.
- To overcome CORS restrictions, the API server needs to include appropriate CORS headers in its responses or support JSONP (JSON with Padding) if available.

## Versioning:

- Web APIs can evolve and introduce changes over time.
- API providers often use versioning to manage changes and ensure backward compatibility for existing clients.
- It is important to be aware of the API version you are using and stay updated with any changes or deprecations announced by the API provider.

## Testing and Debugging:

- It is crucial to test your API integration thoroughly and handle different scenarios, such as successful responses, error responses, edge cases, and network failures.
- Tools like Postman, Insomnia, or browser developer tools can help you test and debug API requests and responses.

### Final Note

Remember, the specific functionality, attributes, events, event types, and intervals available will depend on the specific web API you are working with. It's important to refer to the documentation for the particular API you are using to understand its full capabilities and how to use them effectively.

Working with web APIs requires a good understanding of HTTP, data formats (such as JSON or XML), and JavaScript (or other programming languages) to make API requests and handle responses effectively. Regularly reviewing the API documentation and staying up-to-date with any changes or updates from the API provider is essential for successful API integration.

---

# Questions I asked

## Question

In this snippet, the line of code `element.setAttribute("textContent", "");` is not updating the text content.

```
var container = document.querySelector(".container");

container.addEventListener("click", function (event) {
  var element = event.target;

  // TODO: Complete function

  if (element.matches("div")) {
    // id the specific element, by data-number, to change the data-state to be visible

    let state = element.dataset.state;
    if (state === "hidden") {
      element.dataset.state = "visible";
      element.textContent = element.dataset.number;
    } else {
      element.setAttribute("data-state", "hidden");
      // this does not work
      // element.setAttribute("textContent", "");
      // this does work
      element.textContent = "";

      console.log(element);
    }
  }
});

```

## Answer

- In the provided code snippet, the line `element.setAttribute("textContent", "");` is not updating the element's text content because the `setAttribute()` method is used to set an attribute on the element, not the text content itself.

- To update the text content of an element, you should use the textContent property directly, as shown in `element.textContent = "";`. This property allows you to set the text content of the element to an empty string, effectively removing any existing text.

- On the other hand, `setAttribute("textContent", "")` would attempt to set an attribute called "textContent" on the element, which is not a standard attribute and would not affect the visible text content of the element.

- Therefore, to update the text content of an element, you should use `element.textContent = "";`.

## Question

- Using something like element.attribute what is that syntax called?
- Is dot notation the preferred way to access element properties or content?

## Answer

- The syntax element.attribute is called dot notation. (This is what I was thinking)
- Yes, dot notation is the preferred way to access element properties or content in JavaScript.
  - Dot notation is especially useful when working with standard properties of JavaScript objects, such as textContent, innerHTML, className, etc.
  - For example, to access the text content of an element, you would typically use element.textContent. Similarly, to modify the class name of an element, you would use element.className.
  - However, there are cases where dot notation may not be suitable, such as when the property name is dynamic or contains special characters. In those cases, bracket notation can be used. But in general, dot notation is the preferred and more commonly used syntax for accessing element properties or content.

---
